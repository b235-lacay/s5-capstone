const chai = require('chai');
const{ assert, expect }= require('chai');
const http = require('chai-http');
chai.use(http);

describe('forex_api_test_suite', () => {
	
	it('test_api_get_rates_is_running', (done) => {
		chai.request('http://localhost:5001').get('/forex/getRates')
		.end((err, res) => {
			assert.isDefined(res);
			done();
		})
	})
	
	it('test_api_get_rates_returns_200', (done) => {
		chai.request('http://localhost:5001')
		.get('/forex/rates')
		.end((err, res) => {
			assert.equal(res.status,200)
			done();	
		})		
	})
	

	it('test_api_post_currency_returns_200_if_complete_input_given', () => {
		chai.request('http://localhost:5001')
		.post('/forex/currency')
		.type('json')
		.send({
			alias: 'nuyen',
			name: 'Shadowrun Nuyen',
			ex: {
				'peso': 0.47,
		        'usd': 0.0092,
		        'won': 10.93,
		        'yuan': 0.065
			}
		})
		.end((err, res) => {
			assert.equal(res.status,200)
			
		})
	})

})

// [SECTION] Capstone Project
describe('API Test Suite - Capstone', () => {
    it('[1] TEST API Currency endpoint is running', () => {
        chai.request('http://localhost:5001')
        .post('/currency')
        .type('json')
        .send({
            alias: 'hkd',
	        name: 'Hong kong dollar',
            ex: {
                'peso': 0.48,
                'usd': 0.0093,
                'won': 10.94,
                'yuan': 0.066
            }
        })
        .end((err, res) => {
            expect(res).to.not.equal(undefined);
        })
    })

    it('[2] TEST if post /currency returns status 400 if name is missing', (done) => {
        chai.request('http://localhost:5001')
        .post('/currency')
        .type('json')
        .send({
            alias: 'hkd',
            ex: {
                'peso': 0.48,
                'usd': 0.0093,
                'won': 10.94,
                'yuan': 0.066
            }
        })
        .end((err, res) => {
            expect(res.status).to.not.equal(400);
            done();
        });
    });

    it('[3] TEST if post /currency returns status 400 if name is not a string', (done) => {
        chai.request('http://localhost:5001')
        .post('/currency')
        .type('json')
        .send({
            name: 'Hong Kong dollar'
        })
        .end((err, res) => {
            expect(res.status).to.not.equal(400);
            done();
        });
    })
    
    it('[4] TEST if post /currency returns status 400 if name is empty string', (done) => {
        chai.request('http://localhost:5001')
        .post('/currency')
        .type('json')
        .send({
            name: '',
            alias: 'hkd',
            ex: {
                'peso': 0.48,
                'usd': 0.0093,
                'won': 10.94,
                'yuan': 0.066
            }
        })
        .end((err, res) => {
            expect(res.status).to.not.equal(400);
            done();
        });
    })

    it('[5]TEST  if post /currency returns status 400 if ex is missing ', (done) => {
        chai.request('http://localhost:5001')
        .post('/currency')
        .type('json')
        .send({
            name: 'Hong Kong dollar',
            alias: 'hkd'
        })
        .end((err, res) => {
            expect(res.status).to.not.equal(400);
            done();
        });
    })

    it('[6] TEST if post /currency returns status 400 if ex is not an object ', (done) => {
        chai.request('http://localhost:5001')
        .post('/currency')
        .type('json')
        .send({
            name: 'Hong Kong Dollar',
            alias: 'hkd',
            ex: {
                'peso': 0.48,
                'usd': 0.0093,
                'won': 10.94,
                'yuan': 0.066
            }
        })
        .end((err, res) => {
            expect(res.status).to.not.equal(400);
            done();
        });
    })

    it('[7] TEST if post /currency returns status 400 if ex is empty object ', (done) => {
        chai.request('http://localhost:5001')
        .post('/currency')
        .type('json')
        .send({
            name: 'Hong Kong dollar',
            alias: 'hkd',
            ex: {
                
            }
        })
        .end((err, res) => {
            expect(res.status).to.not.equal(400);
            done();
        });
    })

    it('[8] TEST if post /currency returns status 400 if alias is missing', (done) => {
        chai.request('http://localhost:5001')
        .post('/currency')
        .type('json')
        .send({
            name: 'Hong Kong dollar',
            ex: {
                'peso': 0.48,
                'usd': 0.0093,
                'won': 10.94,
                'yuan': 0.066
            }
        })
        .end((err, res) => {
            expect(res.status).to.not.equal(400);
            done();
        });
    })

    it('[9] TEST if post /currency returns status 400 if alias is not an string', (done) => {
        chai.request('http://localhost:5001')
        .post('/currency')
        .type('json')
        .send({
            name: 'Saudi Arabian Riyadh',
            alias: 1,
            ex: {
                'peso': 0.47,
                'usd': 0.0092,
                'won': 10.93,
                'yuan': 0.065
            }
        })
        .end((err, res) => {
            expect(res.status).to.not.equal(400);
            done();
        });
    })

    it('[10] TEST if post /currency returns status 400 if alias is empty', (done) => {
        chai.request('http://localhost:5001')
        .post('/currency')
        .type('json')
        .send({
            name: 'Hong kong dollar',
            alias: '',
            ex: {
                'peso': 0.48,
                'usd': 0.0093,
                'won': 10.94,
                'yuan': 0.066
            }
        })
        .end((err, res) => {
            expect(res.status).to.not.equal(400);
            done();
        });
    })

    it('[11] Check if post /currency returns status 400 if ex all fields are complete but there is a duplicate alias', (done) => {
        chai.request('http://localhost:5001')
        .post('/currency')
        .type('json')
        .send({
            name: 'Saudi Arabian Riyadh',
            alias: 'riyadh',
            ex: {
                
            }
        })
        .end((err, res) => {
            expect(res.status).to.not.equal(400);
            done();
        });
    })

    it('[12] Check if post /currency returns status 200 if all fields are complete and there are no duplicates', (done) => {
        chai.request('http://localhost:5001')
        .post('/forex/currency')
        .type('json')
        .send({
            alias: 'riyadh',
            name: 'Saudi Arabian Riyadh',
            ex: {
                'peso': 0.47,
                'usd': 0.0092,
                'won': 10.93,
                'yuan': 0.065
            }
        })
        .end((err, res) => {
            expect(res.status).to.equal(200);
            done();
        });
    })
})

